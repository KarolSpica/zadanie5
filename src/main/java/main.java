import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import pl.edu.pjwstk.mpr.lab5.domain.Address;
import pl.edu.pjwstk.mpr.lab5.domain.Person;
import pl.edu.pjwstk.mpr.lab5.domain.User;
import pl.edu.pjwstk.mpr.lab5.service.*;
public class main {

	public static void main(String[] args) {
		
		Address adress1= new Address();
		adress1.setStreetName("Starogardzka");
		adress1.setHouseNumber(59);
		adress1.setFlatNumber(1);
		adress1.setCity("Czersk");
		adress1.setPostCode("89-650");
		adress1.setCountry("Poland");
		
		Address adress2 = new Address();
		adress2.setStreetName("Swierkowa");
		adress2.setHouseNumber(23);
		adress2.setFlatNumber(3);
		adress2.setCity("Bydgoszcz");
		adress2.setPostCode("12-345");
		adress2.setCountry("Poland");
		
		Address adress3 = new Address();
		adress3.setStreetName("Morska");
		adress3.setHouseNumber(7);
		adress3.setFlatNumber(2);
		adress3.setCity("Bydgoszcz");
		adress3.setPostCode("12-345");
		adress3.setCountry("Germany");
		
		List<Address> listAdress = new ArrayList<>();
		listAdress.add(adress1);
		listAdress.add(adress2);
		listAdress.add(adress3);
		
		String number1 = "654234321";
		String number2 = "987345098";
		
		List<String> listNumbers = new ArrayList<>();
		listNumbers.add(number1);
		listNumbers.add(number2);
		
		
		Person personDetails1 = new Person();
		personDetails1.setName("Karol");
		personDetails1.setSurname("Spica");
		personDetails1.setPhoneNumbers(listNumbers);
		personDetails1.setAddresses(listAdress);
		personDetails1.setRole(null);
		personDetails1.setAge(30);
		

		Person personDetails2 = new Person();
		personDetails2.setName("Monika");
		personDetails2.setSurname("Spica");
		personDetails2.setPhoneNumbers(listNumbers);
		personDetails2.setAddresses(listAdress);
		personDetails2.setRole(null);
		personDetails2.setAge(23);
		
		User user1 = new User();
		user1.setName("Loczek");
		user1.setPassword("karolspica");
		user1.setPersonDetails(personDetails1);
		
		User user2 = new User();
		user2.setName("MonikaJur");
		user2.setPassword("monikamilka");
		user2.setPersonDetails(personDetails2);
		
		List<User> users = new ArrayList<>();
		users.add(user1);
		users.add(user2);

		String result = UserService.getNamesAndSurnamesCommaSeparatedOfAllUsersAbove18(users);
		System.out.println("userWithLongestUsername = " + result);


		System.out.println();
		
	}

}
