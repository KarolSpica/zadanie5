package pl.edu.pjwstk.mpr.lab5.service;

import pl.edu.pjwstk.mpr.lab5.domain.Person;
import pl.edu.pjwstk.mpr.lab5.domain.Role;
import pl.edu.pjwstk.mpr.lab5.domain.User;

import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Krzysztof Pawlowski on 22/11/15.
 */
public class UserService {


    public static List<User> findUsersWhoHaveMoreThanOneAddress(List<User> users) {
    	return users.stream()
				.filter(user -> user.getPersonDetails().getPhoneNumbers().size() >1)
				.collect(Collectors.toList());
    }

    public static Person findOldestPerson(List<User> users) {
        return users.stream().map(user -> user.getPersonDetails())
        					.max(Comparator.comparing(person -> person.getAge())).get();
    }

    public static User findUserWithLongestUsername(List<User> users) {

		List<User> sorted = users.stream()
				.sorted((o1, o2) -> o1.getName().compareTo(o2.getName()))
				.collect(Collectors.toList());
		return sorted.get(sorted.size() -1);
    }

    public static String getNamesAndSurnamesCommaSeparatedOfAllUsersAbove18(List<User> users) {
		return users.stream()
				.filter(user -> user.getPersonDetails().getAge() > 18)
				.map(User::getPersonDetails)
				.map(person -> person.getName()+","+ person.getSurname())
				.collect(Collectors.joining(","));
    }

    public static List<String> getSortedPermissionsOfUsersWithNameStartingWithA(List<User> users) {
		return users.stream()
				.filter(user -> user.getName().charAt(0) == 'A')
				.flatMap(user -> user.getPersonDetails().getRole().getPermissions().stream())
				.map(permission -> permission.getName())
				.sorted((o1, o2) -> o1.compareTo(o2))
				.collect(Collectors.toList());
    }

    public static void printCapitalizedPermissionNamesOfUsersWithSurnameStartingWithS(List<User> users) {
		users.stream().filter(user -> user.getPersonDetails().getSurname().charAt(0) == 'S')
				.flatMap(user -> user.getPersonDetails().getRole().getPermissions().stream())
				.map(permission -> permission.getName())
				.forEach(System.out::println);
    }

    public static Map<Role, List<User>> groupUsersByRole(List<User> users) {
		return users
				.stream()
				.collect(Collectors.groupingBy(user -> user.getPersonDetails().getRole()));
    }

    public static Map<Boolean, List<User>> partitionUserByUnderAndOver18(List<User> users) {

		return users
				.stream()
				.collect(Collectors.partitioningBy(user -> user.getPersonDetails().getAge()>18));


    }
}
